+++
title = "My experience"
description = "Where am I going to?"
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 20
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = ""
toc = true
top = false
+++

I was born in China, Shenzhen and lived here. I love to go to the sea and listen to the music "<a href="https://www.youtube.com/watch?v=kBE7IWb7eJE" target="_blank">Summer Cozy Rock</a>" which reminds me of the summer time in Shenzhen.

<img src = "../IMG_4496.jpg" width = 50%> <br> <br> <br>

<img src = "../IMG_4596.jpg" width = 50%> <br> <br> <br>

<img src = "../IMG_4730_2.jpg" width = 50%><br> <br> <br>
