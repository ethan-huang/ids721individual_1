1. ## Deployment ：

   - website: https://ids721-ethan-huang-07e0d350387937bb2f1c2203ce023f1b077409d9577a.gitlab.io/
   - Netlify: https://rococo-capybara-8fbfff.netlify.app/

   ## Demo Video:

   - [Demo Video](https://gitlab.com/ethan-huang/ids721individual_1/-/blob/master/demo.mp4?ref_type=heads)

   ## Features:

   - Homepage: My short introduction about myself and my journey with coding.

   - Personal Info: Learn about my educational background and skill set.

   - myself: some cool coding projects that I am proud of and nice photos of my homeland

   ## Structures:

   - Content: Markdown files for web content.

   - Static: Static assets like images and compiled CSS.

   - Templates: HTML layouts and components.

   - Themes: AdiDoks theme for aesthetic and functionality.

   ## Steps

   - `zola init mysite` to start a simple skeleton about the website

   - `cd mysite/themes && git clone https://github.com/aaranxu/adidoks.git` to add the themes to the template

   - and change the themes = "adidoks"

   - use `zola build && zola serve` to see the website

   ## Deploy

   - The .gitlab-ci.yml file defines the CI/CD process, which builds and updates the site on each push to the GitLab repository.

   - add netlify.toml file and add configuration for deploy on Netlify
