+++
title = "Welcome to the Neverland"


# The homepage contents
[extra]
lead = 'Some dots will connect in the future'
url = "/docs/getting-started/introduction/"
url_button = "Get started"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source MIT License."
repo_url = "https://github.com/aaranxu/adidoks"

# Menu items
[[extra.menu.main]]
name = "Protofolio"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "Coding Journey"
content = 'I printed Hello World back in 2019 at UIUC for the course CS125'

[[extra.list]]
title = "Favorite poems"
content = 'Gather ye rosebuds while ye may,Old Time is still a-flying. And this same flower that smiles today. Tomorrow will be dying.'

[[extra.list]]
title = "Focus area"
content = "I love to solve real-world problems with different algorithms."

[[extra.list]]
title = "Hobbies"
content = "I love playing basketball and badminton. Sports are essential. That is why I love Duke."

[[extra.list]]
title = "Favorite songs"
content = "Merry Christmas Mr.Lawrence. - Ryuichi Sakamoto"

[[extra.list]]
title = "My goal"
content = "To learn more about frontend and API."

+++
